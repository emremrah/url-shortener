"""General purpose helper functions."""
import random
from string import ascii_lowercase, digits


def get_random_string(length=6) -> str:
    """Generate a random string of lowercase letters and digits."""
    chars = ascii_lowercase + digits

    return ''.join(random.choice(chars) for i in range(length))
