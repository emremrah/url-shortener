"""Helper functions for database operations."""
from datetime import datetime
from typing import Dict, List, Optional

from pymongo import MongoClient
from pymongo.database import Database

from urlshortener.helpers import get_random_string


def get_db() -> Database:
    """Get mongodb database object."""
    client = MongoClient('mongodb://localhost:27017/')
    db = client['urlshortener']

    # create unique index on url_key
    db.urls.create_index('url_key', unique=True)

    return db


def generate_url_key(db: Database) -> str:
    """Generate a random, unique URL key."""
    # generate a random string of 6 characters
    url_key = get_random_string()

    while db.urls.find_one({'url_key': url_key}):
        url_key = get_random_string()

    return url_key


def get_url(db: Database, url_key: str) -> Optional[Dict]:
    """Get the URL object for the given URL key."""
    return db.urls.find_one({'url_key': url_key})


def create_url(db: Database, url: str) -> Dict:
    """Create a new URL and return the short URL."""
    # generate a random string of 6 characters
    url_key = generate_url_key(db)

    # create the URL object
    doc = {
        'url': url,
        'url_key': url_key,
        'visits': 0,
        'created_at': datetime.now(),
    }

    db.urls.insert_one(doc)

    return doc


def increment_visit_count(db: Database, url_key: str):
    """Increment the visit count for the given URL key."""
    db.urls.update_one(
        {'url_key': url_key},
        {'$inc': {'visits': 1}}
    )


def get_urls(db: Database) -> List[Dict]:
    """Get all URLs."""
    docs = list(db.urls.find({}, {'_id': 0}))

    return docs
