"""URL shortener app."""
from flask import Flask, jsonify

from .db_helpers import get_db


def create_app():
    """Create and configure the Flask application."""
    app = Flask(__name__)

    @app.route('/', methods=['GET'])
    def default():
        return jsonify({'message': 'the server is up and running'})

    return app


app = create_app()

db = get_db()


from . import api  # noqa
