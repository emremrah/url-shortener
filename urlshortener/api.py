"""URL shortener API."""
import validators
from flask import jsonify, redirect, request

import urlshortener.db_helpers as db_helpers

from . import app, db


@app.route('/api/shorten', methods=['POST'])
def shorten_url():
    """Shorten the given URL and return the short URL."""
    # get target URL from the request body
    try:
        url = request.json['url']
    except KeyError:
        return jsonify({'message': 'no url provided'}), 400

    # check if url is valid
    if not validators.url(url):
        return jsonify({'message': 'invalid URL'}), 400

    # create url
    doc = db_helpers.create_url(db, url)

    return jsonify({'url': 'http://localhost:5000/' + doc['url_key']})


@app.route('/<url_key>', methods=['GET'])
def redirect_to_url(url_key):
    """Redirect to the target URL."""
    # get the target URL from the database
    url = db_helpers.get_url(db, url_key)

    # if the url_key doesn't exist in db, return a 404
    if url is None:
        return jsonify({'message': 'URL not found'}), 404

    # increment the visit count
    db_helpers.increment_visit_count(db, url_key)

    # redirect to the target URL
    return redirect(location=url['url'], code=302)


@app.route('/api/urls', methods=['GET'])
def get_urls():
    """Get all URLs."""
    return jsonify(db_helpers.get_urls(db))
