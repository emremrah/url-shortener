# URL Shortener

A REST API service example to shorten URLs.

## Usage

Install the requirements:

```bash
# python >= 3.8
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

To run the service:

```bash
python wsgi.py --port <PORT>
```

or, to run for production:

```bash
gunicorn -b 0.0.0.0:<PORT> wsgi:app
```

## Roadmap

These features are not difficult to implement.

- [x] Add swagger documentation
- [ ] Add authorization via JWT
- [ ] Add request validation and data models using `pydantic`, `marshmallow` etc.
- [ ] Dockerize with `docker-compose`
  - Simple dockerization is possible using `mongo:latest` and a custom python image.
- [ ] Add tests
