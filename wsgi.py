"""WSGI."""
import click

from urlshortener import app


@click.command()
@click.option('--port', default=5000, help='port to run the server on')
def main(port: int):
    """Run the application."""
    app.run(debug=True, port=port)


if __name__ == '__main__':
    main()
